function isPrime(n) {
    for (let i = 2; i < n; i++){
        if (n % i === 0){
            return false
        }
    }
    return true
}


function snapCrackle(maxValue) {
    let stringFinal = ""
    for (let i = 1; i <= maxValue; i++){
        if (i%2 != 0 && i%5 === 0){
            stringFinal += "SnapCrackle, "
        }else if (i%2 != 0){
            stringFinal += "Snap, "
        }else if (i%5 === 0){
            stringFinal += "Crackle, "
        }else{
            stringFinal += i + ", "
        }
    }
    console.log(stringFinal)    
}


function snapCracklePrime(maxValue) {
    let stringFinal = ""
    for (let i = 1; i <= maxValue; i++){
        if (i % 2 != 0){
            stringFinal += "Snap"
        }
        if (i % 5 === 0){
            stringFinal += "Crackle"
        }
        if (isPrime(i)){
            stringFinal += "Prime"
        }
        if (!(i % 2 != 0) && !(i % 5 === 0) && !(isPrime(i))){
            stringFinal += i
        }
        stringFinal += ", "
    }
    console.log(stringFinal)    
}